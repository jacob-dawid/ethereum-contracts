contract EthereumSplitter {

    struct Recipient {
        address etherAddress;
        uint fraction;
    }

    Recipient[] public recipients;

    address public owner;
    uint public balance;

    modifier onlyowner { if (msg.sender == owner) _ }

    function EthereumSplitter() {
        owner = msg.sender;
    }

    function() {
        enter();
    }
    
    function enter() {
        uint incomingBalance = msg.value;
        if (incomingBalance <= 0 ether) {
            return;
        }

        uint cake = incomingBalance;
        for(uint i = 0;i < recipients.length; i++) {
            uint balanceToSpend = incomingBalance / recipients[i].fraction;
            if(balanceToSpend > 0) {
              recipients[i].etherAddress.send(balanceToSpend);
            }
            cake -= balanceToSpend;
        }

        balance += cake;
    }

    function retrieveFunds() onlyowner {
        owner.send(balance);
        balance = 0;
    }

    function setOwner(address _owner) onlyowner {
        owner = _owner;
    }

    function addRecipient(address recipientAddress, uint fraction) onlyowner {
        if(fraction >= 1) {
          uint idx = recipients.length;
          recipients.length += 1;
          recipients[idx].etherAddress = recipientAddress;
          recipients[idx].fraction = fraction;
        }
    }
}
